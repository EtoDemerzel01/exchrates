﻿# ExchRates training project (Quick Currency online converter)

ExchRates is a closs-platform, mobile-ready web application created for educational purposes.
Deployed on VPS with Ubuntu, launched behind ngnix http server:

* [https://q-c.online:38777/](https://q-c.online:38777/) - Quick Currency Converter


## Objectives:
- ASP.NET Core 2.2
- EF Core 2.2
- Get and parse JSON from external source every 3h
- Save data into local MySQL db
- Responsive front-end (PC, mobile) with jQuery & Bootstrap

App uses external API that returns result in json format.
* [The Free Currency Converter API](https://free.currencyconverterapi.com/) - Source
* [Documentation](https://www.currencyconverterapi.com/docs)

Conversion Pairs per Request for free account is limited to 10. 
Avaliable pairs are:

* RUB to EUR
* RUB to USD
* RUB to THB
* RUB to TRY
* RUB to LKR
* EUR to RUB
* USD to RUB
* THB to RUB
* TRY to RUB
* LKR to RUB

The user can get the calculation of exchange rates for a given date since 01-Jan-2018.
The data is retrieved from the local database (MySQL).

### Notes
First launch of the application will create the new QCurrency database and all db objects needed (EF Core Migrations).
Then the application will load the data using the third party API. Data is updated every three hours.

`ConnectionString` was moved from `appsettings.json` file to the user secret store for security reasons. 
To get web app works locally you need a proper `ConnectionString` for your database.

Connection strings Examples (appsettings.json):

* `"LocalDatabase": "Data Source=WKZN-LEDUKHOVSK\\HELLOWORLD;Initial Catalog=CurrencyDB;Integrated Security=true;MultipleActiveResultSets=true;" // SQL Server`
* `"LocalDatabase": "server=WKZN-Ledukhovski;UserId=username;Password=****;database=QCurrency;" //MySql`


#### License
MIT

Data is provided "as is" without warranty of any kind.