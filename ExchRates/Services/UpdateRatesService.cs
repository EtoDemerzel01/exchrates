﻿using ExchRates.Data;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ExchRates.Services
{
    public class UpdateRatesService : IHostedService
    {
        //every 3h load new rates
        private Timer _timer;
        private readonly IServiceScopeFactory scopeFactory;

        public UpdateRatesService(IServiceScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(ExecUpdate, null, 0, 10800000);
            return Task.CompletedTask;
        }

        void ExecUpdate(object state)
        {
            DateTime endDate = DateTime.Now;
            
            using (var scope = scopeFactory.CreateScope())
            {
                var ctx = scope.ServiceProvider.GetRequiredService<CurrencyCtx>();
                DateTime startDate = CurrencySource.GetLastHistoryDate(ctx).AddDays(1);
                AddData.Rate(ctx, startDate, endDate);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
    }
}
