﻿using System;
using System.Linq;
using ExchRates.Models;

namespace ExchRates.Data
{
    public static class InitData
    {
        //Initial loadig of currency pairs
        public static void CurrencyPairs(CurrencyCtx ctx)
        {
            // Create & insert into CurrencyPairs table
            ctx.Database.EnsureCreated();

            // Runs only if table not loaded yet
            if (ctx.CurrencyPair.Any()) return;

            // just 10 pairs, free account limit
            // https://free.currencyconverterapi.com
            var currpairs = new CurrencyPair[]
            {
                new CurrencyPair{FromCurrencyCode="EUR", ToCurrencyCode="RUB"},
                new CurrencyPair{FromCurrencyCode="LKR", ToCurrencyCode="RUB"},
                new CurrencyPair{FromCurrencyCode="RUB", ToCurrencyCode="EUR"},
                new CurrencyPair{FromCurrencyCode="RUB", ToCurrencyCode="LKR"},
                new CurrencyPair{FromCurrencyCode="RUB", ToCurrencyCode="THB"},
                new CurrencyPair{FromCurrencyCode="RUB", ToCurrencyCode="TRY"},
                new CurrencyPair{FromCurrencyCode="RUB", ToCurrencyCode="USD"},
                new CurrencyPair{FromCurrencyCode="THB", ToCurrencyCode="RUB"},
                new CurrencyPair{FromCurrencyCode="TRY", ToCurrencyCode="RUB"},
                new CurrencyPair{FromCurrencyCode="USD", ToCurrencyCode="RUB"},
            };

            foreach (CurrencyPair currpair in currpairs)
            {
                ctx.CurrencyPair.Add(currpair);
            }

            ctx.SaveChanges();
        }

        //Currency dictionary initial loading if not exist
        public static void Currencies(CurrencyCtx ctx)
        {
            ctx.Database.EnsureCreated();

            if (ctx.Currency.Any()) return;

            foreach (Currency currency in CurrencySource.GetCurrencies())
            {
                ctx.Currency.Add(currency);
            }

            ctx.SaveChanges();
        }
    }

    public static class AddData
    {
        public static void Rate (CurrencyCtx ctx, ExchangeRate exchRate)
        {
            ctx.ExchangeRate.Add(exchRate);
        }

        public static void Rate (CurrencyCtx ctx, DateTime startDate, DateTime? endDate = null)
        {
            DateTime nextDate = startDate;
            while (nextDate <= (endDate ?? startDate))
            {
                foreach (var exchrate in CurrencySource.GetRates(ctx, nextDate))
                {
                    Rate(ctx ,exchrate);
                }
                nextDate = nextDate.AddDays(1);
            }
            ctx.SaveChanges();
        }
    }

}
