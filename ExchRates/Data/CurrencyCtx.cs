﻿using ExchRates.Models;
using Microsoft.EntityFrameworkCore;

namespace ExchRates.Data
{
    public class CurrencyCtx : DbContext
    {
        public CurrencyCtx(DbContextOptions<CurrencyCtx> options) : base(options)
        {
        }

        public DbSet<Currency> Currency { get; set; }
        public DbSet<ExchangeRate> ExchangeRate { get; set; }
        public DbSet<CurrencyPair> CurrencyPair { get; set; }
        // test

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Primary keys
            modelBuilder.Entity<Currency>()
                .HasKey(currency => currency.CurrencyId);
            modelBuilder.Entity<ExchangeRate>()
                .HasKey(exchrate => new { exchrate.ValidDate, exchrate.FromCurrencyCode, exchrate.ToCurrencyCode });
            modelBuilder.Entity<CurrencyPair>()
                .HasKey(curpair => new { curpair.FromCurrencyCode, curpair.ToCurrencyCode });

            // Foreign keys
            modelBuilder.Entity<ExchangeRate>()
                .HasOne(exchrate => exchrate.CurrencyFrom)
                .WithMany(currency => currency.ExchangeRateFrom)
                .HasForeignKey(exchrate => exchrate.FromCurrencyCode)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ExchangeRate>()
                .HasOne(exchrate => exchrate.CurrencyTo)
                .WithMany(currency => currency.ExchangeRateTo)
                .HasForeignKey(exchrate => exchrate.ToCurrencyCode)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<CurrencyPair>()
                .HasOne(curpair => curpair.CurrencyFrom)
                .WithMany(currency => currency.CurrencyPairFrom)
                .HasForeignKey(curpair => curpair.FromCurrencyCode)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<CurrencyPair>()
                .HasOne(curpair => curpair.CurrencyTo)
                .WithMany(currency => currency.CurrencyPairTo)
                .HasForeignKey(curpair => curpair.ToCurrencyCode)
                .OnDelete(DeleteBehavior.Restrict);

            // Currency entity
            modelBuilder.Entity<Currency>()
                .ToTable("Currencies")
                .Property(currency => currency.CurrencyId).HasColumnName("Code")
                .HasMaxLength(3)
                .IsRequired();
            modelBuilder.Entity<Currency>()
                .Property(currency => currency.CurrencyName).HasColumnName("Name")
                .HasMaxLength(100)
                .IsRequired();
            modelBuilder.Entity<Currency>()
                .Property(currency => currency.CurrencySymbol).HasColumnName("Symbol")
                .IsUnicode();

            // EchangeRate entity
            modelBuilder.Entity<ExchangeRate>()
                .ToTable("ExchangeRates")
                .Property(exchrate => exchrate.ValidDate).HasColumnType("date")
                .IsRequired();
            modelBuilder.Entity<ExchangeRate>()
                .Property(exchrate => exchrate.FromCurrencyCode).HasMaxLength(3)
                .IsRequired();
            modelBuilder.Entity<ExchangeRate>()
                .Property(exchrate => exchrate.ToCurrencyCode).HasMaxLength(3)
                .IsRequired();
            modelBuilder.Entity<ExchangeRate>()
                .Property(exchrate => exchrate.ExchRate).HasColumnType("double")
                .IsRequired();

            // CurrencyPair entity
            modelBuilder.Entity<CurrencyPair>()
                .ToTable("CurrencyPairs")
                .Property(curpair => curpair.FromCurrencyCode).HasMaxLength(3)
                .IsRequired();
            modelBuilder.Entity<CurrencyPair>()
                .Property(curpair => curpair.ToCurrencyCode).HasMaxLength(3)
                .IsRequired();
        }
    }
}
