﻿using ExchRates.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;

namespace ExchRates.Data
{
    public class CurrencySource
    {       
        public static readonly string SourceUrl = "https://free.currencyconverterapi.com";
        static readonly string ApiKey = "a32f51b0905e96ac5f6a";

        //build url for quering source
        public static string GetUrl(string fromDateStr, string currPairsStr = null, string toDateStr = null)
        {
            // for one day result url should be: https://../api/v6/convert?q=USD_PHP,PHP_USD&compact=ultra&date=[yyyy-mm-dd]&apiKey=[YOUR_API_KEY]
            string result = SourceUrl + "/api/v6/convert?q=" + currPairsStr + "&compact=ultra&date=" + fromDateStr + "&apiKey=" + ApiKey;
            return result;
        }

        //returns whole list of known currencies from currencyconverterapi
        public static List<Currency> GetCurrencies()
        {
            // http query: https://../api/v6/currencies?apiKey=[YOUR_API_KEY]
            string url = SourceUrl + "/api/v6/currencies?apiKey=" + ApiKey;
            using (var client = new WebClient())
            {
                string json_data = client.DownloadString(url);
                JToken[] data = JObject.Parse(json_data)["results"].ToArray();
                return data.Select(item => item.First.ToObject<Currency>()).ToList();
            }
        }

        //returns result string in format "USD_PHP,PHP_USD,..." for constructing correct url
        public static string GetCurrencyPairStr(CurrencyCtx ctx)
        {
            var currencyPairs = new List<CurrencyPair>();
            string result = "";

            try
            {
                currencyPairs = ctx.CurrencyPair.ToList();
            }
            catch (InvalidOperationException)
            {
                return result;
            }
            
            foreach (CurrencyPair currPair in currencyPairs)
            {
                result = (result != "" ? result + "," : "") + currPair.FromCurrencyCode + "_" + currPair.ToCurrencyCode;
            }

            return result;
        }

        //last record in history table
        public static DateTime GetLastHistoryDate (CurrencyCtx ctx)
        {
            DateTime? result = new DateTime();
            result = ctx.ExchangeRate.Max(history => (DateTime?)history.ValidDate);

            return result ?? new DateTime(DateTime.Now.Year, 1, 1); 
        }

        //query source for rate for one date all currency pairs
        public static List<ExchangeRate> GetRates (CurrencyCtx ctx, DateTime validDate)
        {
            using (var client = new WebClient())
            {
                var url = GetUrl(validDate.ToString("yyyy-MM-dd"), GetCurrencyPairStr(ctx));
                Debug.WriteLine($"Quering remote server, url: {url}");
                string json_data = client.DownloadString(url);
                JObject data = JObject.Parse(json_data);
                return data.Properties().Select(prop => new ExchangeRate
                {
                    ValidDate = validDate,
                    FromCurrencyCode = prop.Name.Substring(0, 3),
                    ToCurrencyCode = prop.Name.Substring(4, 3),
                    ExchRate = data[prop.Name][validDate.ToString("yyyy-MM-dd")].ToObject<double>(),
                }).ToList();
            }
        }
    }
}
