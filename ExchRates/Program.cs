﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using ExchRates.Data;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.EntityFrameworkCore;

namespace ExchRates
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var ctx = services.GetRequiredService<CurrencyCtx>();
                    ctx.Database.Migrate();

                    DateTime startDate = CurrencySource.GetLastHistoryDate(ctx).AddDays(1);
                    DateTime endDate = DateTime.Now;

                    InitData.Currencies(ctx);
                    InitData.CurrencyPairs(ctx);
                }
                catch (System.Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while loading initial data.");
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();

    }
}
