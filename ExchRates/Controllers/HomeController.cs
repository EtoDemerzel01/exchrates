﻿using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using ExchRates.Models;
using ExchRates.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;

namespace ExchRates.Controllers
{
    public class HomeController : Controller
    {
        private readonly CurrencyCtx _ctx;

        public HomeController(CurrencyCtx ctx)
        {
            _ctx = ctx;
        }

        public IActionResult Index()
        {
            var vm = new ConverterViewModel();
            vm.CurrencyPairs = new List<CurrencyPair>();
            vm.CurrencyPairs = (from currency in _ctx.CurrencyPair select currency).ToList();
            vm.ValidDate = CurrencySource.GetLastHistoryDate(_ctx);

            return View(vm);
        }

        public JsonResult GetCurrenciesTo(string FromCurrencyCode)
        {
            var curPairList = new List<CurrencyPair>();
            curPairList = (from currency in _ctx.CurrencyPair where currency.FromCurrencyCode == FromCurrencyCode select currency).ToList();

            return Json(new SelectList(curPairList, "ToCurrencyCode", "ToCurrencyCode"));
        }

        public decimal GetConvertedValue(string FromCurrencyCode, string ToCurrencyCode, DateTime validDate, ulong inputValue)
        {
            decimal result;
            try
            {
                result = Math.Round((decimal)((from exchrate in _ctx.ExchangeRate
                                               where exchrate.FromCurrencyCode == FromCurrencyCode
                                               && exchrate.ToCurrencyCode == ToCurrencyCode
                                               && exchrate.ValidDate == validDate
                                               select exchrate).First().ExchRate * inputValue), 2, MidpointRounding.ToEven);
            }
            catch (InvalidOperationException)
            {
                result = -1M;
            }
            return result;
        }

        public string GetStrRate(string FromCurrencyCode, string ToCurrencyCode, DateTime validDate)
        {
            string result;
            try
            {
                ExchangeRate record = (from rate in _ctx.ExchangeRate
                                       where rate.FromCurrencyCode == FromCurrencyCode
                                       && rate.ToCurrencyCode == ToCurrencyCode
                                       && rate.ValidDate == validDate
                                       select rate).First();
                result = "1 " + record.FromCurrencyCode + " = " + record.ExchRate.ToString() + " " + record.ToCurrencyCode + " Rates of exchange " + record.ValidDate.ToString("dddd, dd MMMM yyyy");
            }
            catch (InvalidOperationException)
            {
                result = "Rate " + FromCurrencyCode + "/" + ToCurrencyCode + " is not available for the date " + validDate.ToString("dddd, dd MMMM yyyy");
            }
            return result;
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
