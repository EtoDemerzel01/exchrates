﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ExchRates.Models
{
    public class Currency
    {
        [JsonProperty("currencyName")]
        public string CurrencyName { get; set; }

        [JsonProperty("currencySymbol")]
        public string CurrencySymbol { get; set; }

        [JsonProperty("Id")]
        public string CurrencyId { get; set; }

        // nav properties
        public virtual List<ExchangeRate> ExchangeRateFrom { get; set; }
        public virtual List<ExchangeRate> ExchangeRateTo { get; set; }
        public virtual List<CurrencyPair> CurrencyPairFrom { get; set; }
        public virtual List<CurrencyPair> CurrencyPairTo { get; set; }

        public static explicit operator string(Currency v)
        {
            throw new NotImplementedException();
        }
    }
}
