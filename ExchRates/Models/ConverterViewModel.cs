﻿using System;
using System.Collections.Generic;

namespace ExchRates.Models
{
    public class ConverterViewModel
    {
        public string FromCurrencyCode { get; set; }
        public string ToCurrencyCode { get; set; }
        public DateTime ValidDate { get; set; }
        public decimal Result { get; set; }
        public List<CurrencyPair> CurrencyPairs { get; set; }        
    }
}
