﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchRates.Models
{
    public class ExchangeRate
    {
        public DateTime ValidDate { get; set; }
        public string FromCurrencyCode { get; set; } // fk
        public string ToCurrencyCode { get; set; } //fk
        public double ExchRate { get; set; }

        public virtual Currency CurrencyFrom { get; set; }
        public virtual Currency CurrencyTo { get; set; }
    }
}
