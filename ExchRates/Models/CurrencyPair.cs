﻿namespace ExchRates.Models
{
    public class CurrencyPair
    {
        public string FromCurrencyCode { get; set; }
        public string ToCurrencyCode { get; set; }

        public virtual Currency CurrencyFrom { get; set; }
        public virtual Currency CurrencyTo { get; set; }
    }
}
