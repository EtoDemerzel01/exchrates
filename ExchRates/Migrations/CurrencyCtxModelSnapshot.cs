﻿// <auto-generated />
using System;
using ExchRates.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ExchRates.Migrations
{
    [DbContext(typeof(CurrencyCtx))]
    partial class CurrencyCtxModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity("ExchRates.Models.Currency", b =>
                {
                    b.Property<string>("CurrencyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Code")
                        .HasMaxLength(3);

                    b.Property<string>("CurrencyName")
                        .IsRequired()
                        .HasColumnName("Name")
                        .HasMaxLength(100);

                    b.Property<string>("CurrencySymbol")
                        .HasColumnName("Symbol")
                        .IsUnicode(true);

                    b.HasKey("CurrencyId");

                    b.ToTable("Currencies");
                });

            modelBuilder.Entity("ExchRates.Models.CurrencyPair", b =>
                {
                    b.Property<string>("FromCurrencyCode")
                        .HasMaxLength(3);

                    b.Property<string>("ToCurrencyCode")
                        .HasMaxLength(3);

                    b.HasKey("FromCurrencyCode", "ToCurrencyCode");

                    b.HasIndex("ToCurrencyCode");

                    b.ToTable("CurrencyPairs");
                });

            modelBuilder.Entity("ExchRates.Models.ExchangeRate", b =>
                {
                    b.Property<DateTime>("ValidDate")
                        .HasColumnType("date");

                    b.Property<string>("FromCurrencyCode")
                        .HasMaxLength(3);

                    b.Property<string>("ToCurrencyCode")
                        .HasMaxLength(3);

                    b.Property<double>("ExchRate")
                        .HasColumnType("double");

                    b.HasKey("ValidDate", "FromCurrencyCode", "ToCurrencyCode");

                    b.HasIndex("FromCurrencyCode");

                    b.HasIndex("ToCurrencyCode");

                    b.ToTable("ExchangeRates");
                });

            modelBuilder.Entity("ExchRates.Models.CurrencyPair", b =>
                {
                    b.HasOne("ExchRates.Models.Currency", "CurrencyFrom")
                        .WithMany("CurrencyPairFrom")
                        .HasForeignKey("FromCurrencyCode")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ExchRates.Models.Currency", "CurrencyTo")
                        .WithMany("CurrencyPairTo")
                        .HasForeignKey("ToCurrencyCode")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ExchRates.Models.ExchangeRate", b =>
                {
                    b.HasOne("ExchRates.Models.Currency", "CurrencyFrom")
                        .WithMany("ExchangeRateFrom")
                        .HasForeignKey("FromCurrencyCode")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ExchRates.Models.Currency", "CurrencyTo")
                        .WithMany("ExchangeRateTo")
                        .HasForeignKey("ToCurrencyCode")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}
